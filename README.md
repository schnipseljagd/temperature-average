# temperature-average service

It's a sunny day.

A simple RESTful weather service.

The service’s purpose is to aggregate current weather information from several other services available 
on the web, and provide averages over the acquired data.
The service includes only a single API endpoint at the moment.
It gets the current temperature in degrees Celsius, taking city name as a parameter.
The response may include attribution for the data if requested by an upstream provider.
Interacting with the API could look like:

Request:

    GET /api/weather/lodz,poland HTTP/1.1

Response:

    200 OK
    Content-Type: application/json
    {
        "temperature": 15.6,
        "credit": "weather.example.org,weather.example.net"
    }

It can source the weather data from one or more free services, like openweathermap.org, Weather
Underground, or forecast.io.
Currently implemented is only openweathermap.org.

It catches some obvious errors in the application, like:

 * Returned temperature can’t go below absolute zero
 * Responds with `HTTP 404` if the queried city does not exist.
 * Responds with `HTTP 503` if all weather platform queries are failing.

## Documentation

The decisions made while building this service are recorded as [architecture decision records](doc/adr/).

## Usage

    # run the tests
    lein test

    # run a standalone jar
    lein uberjar
    java -jar target/temperature-average.jar

## Production

To actually ask the real weather platform for the temperature:

    export CLJ_ENV=prod
    export OPENWEATHERMAPORG_API_KEY=<my-api-key>
    java -jar target/temperature-average.jar
