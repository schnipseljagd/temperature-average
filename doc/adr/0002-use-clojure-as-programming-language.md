# 2. Use Clojure as programming language

Date: 2017-11-28

## Status

Accepted

## Context

The goal is to build a service which provides an average temperature for a city provided by different weather platforms.
It should collect the temperature data in parallel and ignore failing platforms to be able to provide a result in a reasonable amount of time.
Depending on the weather platform it may have to do a geo lookup before being able to query the platform.

## Decision

I made the decision to use clojure for this.

## Consequences

 * The clojure toolset allows me to develop and deploy a prototype very quickly since it's the environment I am most familiar with.
 * Using clojure aligns with the goal to build a fast and robust app.
 * It comes with excellent support for asynchronous workflows. Although the workflow is very simple at the moment, this might become handy later on.
 * I might run into the problem that my reviewers are not very familiar with the language. So it is important to inform them early about the decision and maybe review the choice.
