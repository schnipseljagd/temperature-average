# 3. adding new temperature providers should be easy

Date: 2017-11-28

## Status

Accepted

## Context

One of the goals of the service is to be able to easily integrate new temperature provider.

## Decision

Every provider specific code should be in a namespace in [src/temperature_average/provider](../../src/temperature_average/provider) and not leak into the rest of the code.
Adding a new provider needs three new functions which can be copied from one of the existing provider.

Additionally a new provider has to be registered to the provider lists (prod/stub) [service](../../src/temperature_average/service.clj).

## Consequences

 * Provider can be easily tested without the need to mock side-effects.
 * Unit-testing is extremely easy because it only raw data transformations (input/output) need to be tested.
 * The code is a bit more complex than it needs to be because of the focus on future extensibility.
