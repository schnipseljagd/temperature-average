# 4. use heroku as platform to run on

Date: 2017-11-29

## Status

Accepted

## Context

I need a platform which allows me to run my service without troubling me about details.
Also it should let me scale my service.

## Decision

The decision to use Heroku for me was pretty straightforward since I used it in the past for similar tasks.
It also scales very well up to high traffic load.

## Consequences

 * Heroku brings an easy deployment for clojure apps, basic logging, metrics etc.
 * It lets me scale my service easily.
 * In a context of a company ecosystem heroku might be a bit limiting compared to AWS/GCE.
 * Also it brings a vendor lock in which is not always appreciated.
