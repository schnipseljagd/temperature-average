# 5. Use bitpucket pipeline for CI/CD

Date: 2017-11-29

## Status

Accepted

## Context

I need a simple pipeline to deploy to my heroku service.
It should run my tests and cancel the deployment if they are broken.

## Decision

While creating the repository I saw that bitbucket offers a pipeline feature for CI/CD
and I decided to give it a try.

## Consequences

 * Bitbucket pipelines are pretty simple and straightforward.
   The pipeline runs the tests, does a build and then triggers a Heroku deployment.
 * The Bitbucket pipeline also collects the build outputs which might become handy later.
 * If I need something more complex I should easily be able to migrate to something else.
 * One drawback is that this setup with Heroku/Bitbucket (it is also a Heroku problem)
   doesn't allow me to deploy the tested artifact (in my case a jar file)
   but Heroku builds a new artifact which is then deployed.
   This could be a future requirement.
