(ns util.number)

(defn parse-double [^String str]
  (try (Double/parseDouble str)
       (catch Exception e nil)))

(defn to-precision-2 [number]
  (util.number/parse-double (format "%.2f" number)))
