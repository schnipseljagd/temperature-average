(ns util.environment)

(defn get-envar [varname default]
  (get (System/getenv) varname default))

(defn dev-environment? []
  (= "dev" (get-envar "CLJ_ENV" "dev")))
