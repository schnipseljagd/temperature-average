(ns util.temperature)

(def absolute-zero-degree-celsius -273.15)

(defn to-celsius [k]
  (+ k absolute-zero-degree-celsius))

(defn lesser-absolute-zero? [degree-celsius]
  (< degree-celsius absolute-zero-degree-celsius))
