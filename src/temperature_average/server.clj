(ns temperature-average.server
  (:require [compojure.route :refer [not-found]]
            [ring.middleware.json :refer [wrap-json-response]]
            [compojure.core :refer [defroutes GET context]]
            [org.httpkit.server :refer [run-server]]
            [ring.util.http-response :refer [ok service-unavailable]]
            [temperature-average.service :refer [average-temperature providers] :as service]
            [util.environment :refer [get-envar]])
  (:gen-class))

(defroutes all-routes
           (GET ["/api/weather/:city" :city #"[^/]+"] [city]
             (let [{:keys [error]
                    :as response} (average-temperature (providers city))]
               (case error
                 ::service/all-failing (service-unavailable response)
                 ::service/city-not-found (not-found response)
                 (ok response))))
           (not-found nil))


(defonce server (atom nil))

(defn start []
  (let [s (run-server (-> all-routes
                          (wrap-json-response))
                      {:port (Integer/parseInt (get-envar "PORT" "8080"))})]
    (reset! server s)))

(defn stop []
  (when-not (nil? @server)
    (@server :timeout 100))                                 ;; wait 100ms for existing requests to be finished
  (reset! server nil))

(defn reset []
  (stop)
  (start))

(defn -main []
  (start))
