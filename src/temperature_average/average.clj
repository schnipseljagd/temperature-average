(ns temperature-average.average
  (:require [util.temperature :refer [lesser-absolute-zero?
                                      absolute-zero-degree-celsius]]))

(defn avg [numbers]
  (/ (reduce + numbers) (count numbers)))

(defn calculate-avg [numbers]
  (let [degree-celsius (avg numbers)]
    (if (lesser-absolute-zero? degree-celsius)
      absolute-zero-degree-celsius
      degree-celsius)))
