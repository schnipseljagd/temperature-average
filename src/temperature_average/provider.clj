(ns temperature-average.provider
  (:require [org.httpkit.client :as http]))

(defmulti prepare-request :name)

(defmethod prepare-request :default [{:keys [url name timeout]}]
  [::http-get url])

(defmulti response-body->temperature :name)

(defmethod response-body->temperature :default [{:keys [name body]}]
  (throw (ex-info (format "No method for name %s exists." name)
                  {})))

(defn- do-request [{:keys [timeout] :as params}]
  (let [[action opt] (prepare-request params)
        response-future (case action
                          ::http-get (http/get opt {:timeout timeout})
                          ::no-action opt)]
    [params response-future]))

(defn- params-set-error [params reason]
  (assoc params :error reason))

(defn- params-set-temperature [params temperature]
  (assoc params :temperature temperature))

(defn- handle-response [[{:keys [name expected-http-status]
                          :as   params}
                         response-future]]
  (let [{:keys [error body status] :as response} @response-future] ; wait for response
    (cond
      (not (contains? expected-http-status status)) (params-set-error params
                                                                      ::expected-http-status-not-matched)
      (not (nil? error)) (params-set-error params
                                           ::connection-error)
      :default (let [[status result] (response-body->temperature {:name name :body body})]
                 (if (= status ::ok)
                   (params-set-temperature params result)
                   (params-set-error params status))))))

(defn request-temperatures
  "Requests temperatures from multiple providers in parallel."
  [providers]
  (->> providers
       (map do-request)
       (doall)                                              ; trigger http requests
       (map handle-response)))

(defn request-temperature
  "Requests temperature from one provider."
  [provider]
  (-> provider
      (do-request)
      (handle-response)))

(defn make [name city url description]
  {:name                 name
   :city                 city
   :description          description
   :url                  url
   :expected-http-status #{200 404}
   :timeout              1000})
