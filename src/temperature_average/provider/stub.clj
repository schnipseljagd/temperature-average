(ns temperature-average.provider.stub
  (:require [temperature-average.provider :as provider]))

(defmethod provider/prepare-request :stub [_]
  [::provider/no-action (deliver (promise) {:status 200})])

(defmethod provider/response-body->temperature :stub [_] [::provider/ok 15.6])

(defn make [description]
  (provider/make :stub
                 "Łódź,Poland"
                 "http://weather.example.com"
                 description))

(defmethod provider/prepare-request :failing-stub [_]
  [::provider/no-action (deliver (promise) {:status 500})])

(defmethod provider/response-body->temperature :failing-stub [_] [::provider/ok nil])

(defn make-failing [description]
  (provider/make :failing
                 "Łódź,Poland"
                 "http://weather.example.com"
                 description))


(defmethod provider/prepare-request :city-not-found-stub [_]
  [::provider/no-action (deliver (promise) {:status 404})])

(defmethod provider/response-body->temperature :city-not-found-stub [_] [::provider/city-not-found nil])

(defn make-city-not-found [description]
  (provider/make :city-not-found-stub
                 "foobar"
                 "http://weather.example.com"
                 description))
