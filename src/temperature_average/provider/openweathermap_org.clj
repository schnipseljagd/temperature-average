(ns temperature-average.provider.openweathermap-org
  (:require [temperature-average.provider :as provider]
            [org.httpkit.client :as http]
            [cheshire.core :refer [parse-string]]
            [util.temperature :refer [to-celsius]]
            [util.number :refer [to-precision-2]]
            [util.environment :refer [get-envar]]))

(def api-key (get-envar "OPENWEATHERMAPORG_API_KEY" ""))

(def url-template "https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s")

(defmethod provider/prepare-request :openweathermap-org [{:keys [url city]}]
  [::provider/http-get (format url city api-key)])

(defmethod provider/response-body->temperature :openweathermap-org [{:keys [body]}]
  (let [body (parse-string body)]
    (if (= (get body "cod") "404")
      [::provider/city-not-found nil]
      [::provider/ok
       (-> body
           (get-in ["main" "temp"])
           (to-celsius)
           (to-precision-2))])))

(defn make [city]
  (provider/make :openweathermap-org
                 city
                 url-template
                 "openweathermap.org"))
