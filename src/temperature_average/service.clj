(ns temperature-average.service
  (:require [temperature-average.provider :as provider]
            [temperature-average.provider.stub :as stub]
            [temperature-average.average :refer [calculate-avg]]
            [util.environment :refer [dev-environment?]]
            [temperature-average.provider.openweathermap-org :as openweathermap-org]))

; add provider for testing here
(defn stub-providers []
  [(stub/make "weather.example.org")
   (stub/make-failing "foo.example.net")
   (stub/make "weather.example.net")
   (stub/make-city-not-found "bar.example.net")])

; add provider which do requests through the network here
(defn prod-providers [city]
  [(openweathermap-org/make city)])

(defn providers [city]
  (if (dev-environment?)
    (stub-providers)
    (prod-providers city)))

(defn- error-response [error]
  {:temperature nil
   :credit      []
   :error       error})

(defn- successful-response [responses]
  {:temperature (->> responses
                     (map :temperature)
                     (calculate-avg))
   :credit      (->> responses
                     (map :description)
                     (clojure.string/join ","))})

(defn- successful-responses [responses]
  (remove :error responses))

(defn- city-not-found-errors [responses]
  (filter #(= (:error %) ::provider/city-not-found) responses))

(defn average-temperature [providers]
  (let [responses (provider/request-temperatures providers)
        successful-responses (successful-responses responses)
        city-not-found-responses (city-not-found-errors responses)]
    (cond (not (empty? successful-responses)) (successful-response successful-responses)
          (not (empty? city-not-found-responses)) (error-response ::city-not-found)
          :else (error-response ::all-failing))))
