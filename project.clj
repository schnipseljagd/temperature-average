(defproject temperature-average "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [http-kit "2.2.0"]
                 [compojure "1.6.0"]
                 [metosin/ring-http-response "0.9.0"]
                 [ring/ring-json "0.4.0"]]

  :min-lein-version "2.0.0"
  :uberjar-name "temperature-average.jar"

  :profiles {:uberjar {:aot          :all
                       :jvm-opts     ["-Dclojure.compiler.direct-linking=true"]
                       :main         temperature-average.server}
             :dev {:dependencies [[http-kit.fake "0.2.1"]]}})
