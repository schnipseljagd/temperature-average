(ns temperature-average.service-test
  (:require [clojure.test :refer :all]
            [org.httpkit.fake :refer [with-fake-http]]
            [temperature-average.service :refer [average-temperature providers] :as service]
            [temperature-average.provider.stub :as stub]))

(let [result (average-temperature (providers "Łódź,Poland"))]
  (deftest test-average-temperature
    (is (= 15.6 (:temperature result))))

  (deftest test-credits
    (is (= "weather.example.org,weather.example.net" (:credit result)))))

; all providers are failing
(let [result (average-temperature [(stub/make-failing "foo.example.net")])]
  (deftest test-fields-are-empty
    (is (nil? (:temperature result)))
    (is (empty? (:credits result))))

  (deftest test-error-is-all-failing
    (is (= ::service/all-failing (:error result)))))


; all providers are failing but one contains a city not found error
(let [result (average-temperature [(stub/make-failing "foo.example.net")
                                   (stub/make-city-not-found "bar.example.net")])]
  (deftest test-fields-are-empty
    (is (nil? (:temperature result)))
    (is (empty? (:credits result))))

  (deftest test-error-is-city-not-found
    (is (= ::service/city-not-found (:error result)))))
