(ns temperature-average.provider.openweathermap-org-test
  (:require [clojure.test :refer :all]
            [org.httpkit.fake :refer [with-fake-http]]
            [temperature-average.provider :refer [prepare-request response-body->temperature] :as provider]
            [temperature-average.provider.openweathermap-org :refer :all]))

(def example-city "London,uk")

(def response-body "{\"coord\":{\"lon\":139,\"lat\":35},\n\"sys\":{\"country\":\"JP\",\"sunrise\":1369769524,\"sunset\":1369821049},\n\"weather\":[{\"id\":804,\"main\":\"clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\n\"main\":{\"temp\":289.5,\"humidity\":89,\"pressure\":1013,\"temp_min\":287.04,\"temp_max\":292.04},\n\"wind\":{\"speed\":7.31,\"deg\":187.002},\n\"rain\":{\"3h\":0},\n\"clouds\":{\"all\":92},\n\"dt\":1369824698,\n\"id\":1851632,\n\"name\":\"Shuzenji\",\n\"cod\":200}")

(deftest test-prepare-request
  (let [[action url] (prepare-request (make example-city))]
    (is (= (.contains url example-city)))
    (is (= ::provider/http-get action))))

(deftest test-returns-temperature
  (let [[status temperature] (response-body->temperature {:name :openweathermap-org
                                                          :body response-body})]
    (is (= ::provider/ok status))
    (is (= 16.35 temperature))))

(deftest test-returns-not-found-error
  (let [[status temperature] (response-body->temperature {:name :openweathermap-org
                                                          :body "{\"cod\":\"404\"}"})]
    (is (= ::provider/city-not-found status))
    (is (nil? temperature))))
