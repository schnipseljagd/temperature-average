(ns temperature-average.average-test
  (:require [clojure.test :refer :all]
            [temperature-average.average :refer :all]))

(deftest test-calculates-avg
  (is (= 9 (calculate-avg [4 6 12 14]))))

(deftest test-adjusts-average-when-below-absolute-zero
  (is (= -273.15 (calculate-avg [-273.16 -273.15])))
  (is (= -273.15 (calculate-avg [-273.15 -273.15]))))
