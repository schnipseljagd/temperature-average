(ns temperature-average.provider-test
  (:require [clojure.test :refer :all]
            [org.httpkit.fake :refer [with-fake-http]]
            [temperature-average.provider :as provider]
            [cheshire.core :refer [generate-string parse-string]]
            [util.number :refer [parse-double]])
  (:import (clojure.lang ExceptionInfo)))

(def example-com (provider/make :example-com
                                "Łódź,Poland"
                                "http://example.com/api"
                                "example.com"))

(def openweathermap-org (provider/make :openweatherfake
                                       "Łódź,Poland"
                                       "http://example.com/api2"
                                       "openweather.org"))
(def example-com-temperature 23.0)
(def example-com-request-error [(:url example-com) {:error "foo"}])
(def example-com-response-500 [(:url example-com) {:status 500 :body ""}])
(def example-com-response-ok [(:url example-com)
                              (str example-com-temperature)])

(def openweathermap-org-temperature 76.5)
(def openweathermap-org-response-ok [(:url openweathermap-org)
                                     (generate-string {"main" {"temp" openweathermap-org-temperature}})])

(defmethod provider/response-body->temperature :example-com [{:keys [body]}]
  [::provider/ok
   (-> body
       (parse-double))])

(defmethod provider/response-body->temperature :openweatherfake [{:keys [body]}]
  [::provider/ok
   (-> body
       (parse-string)
       (get-in ["main" "temp"]))])

(defmethod provider/response-body->temperature :response-handler-error [_]
  [:error nil])

(def providers [example-com
                openweathermap-org])

(deftest test-response-handler-returns-failure-reason
  (let [fake-url "fake"
        provider (provider/make :response-handler-error "" fake-url "")]
    (with-fake-http
      [fake-url ""]
      (let [{:keys [error]} (provider/request-temperature provider)]
        (is (= :error error))))))

(deftest test-response-handler-does-not-exist
  (let [fake-url "fake"
        provider (provider/make :foo "" fake-url "")]
    (with-fake-http
      [fake-url ""]
      (is (thrown? ExceptionInfo (provider/request-temperature provider))))))

(deftest test-provider-responds-with-500
  (with-fake-http
    example-com-response-500
    (let [{:keys [error
                  temperature]} (provider/request-temperature example-com)]
      (is (nil? temperature))
      (is (= ::provider/expected-http-status-not-matched error)))))

(deftest test-provider-responds-with-error
  (with-fake-http
    example-com-request-error
    (let [{:keys [error
                  temperature]} (provider/request-temperature example-com)]
      (is (nil? temperature))
      (is (= ::provider/connection-error error)))))

(deftest test-provider-responds-with-temperature
  (with-fake-http
    example-com-response-ok
    (let [{:keys [error
                  temperature]} (provider/request-temperature example-com)]
      (is (= example-com-temperature temperature)))))

(deftest test-provider-has-description
  (with-fake-http
    example-com-response-ok
    (let [{:keys [description]} (provider/request-temperature example-com)]
      (is (= "example.com" description)))))

(deftest test-a-request-failed
  (with-fake-http
    (into
      example-com-request-error
      openweathermap-org-response-ok)
    (let [responses (provider/request-temperatures providers)
          {:keys [error]} (first responses)]
      (is (= ::provider/connection-error error)))))

(deftest test-returns-responses-from-multiple-providers
  (with-fake-http
    (into
      example-com-response-ok
      openweathermap-org-response-ok)
    (let [responses (provider/request-temperatures providers)]
      (is (= 2 (count responses))))))


(deftest test-returns-temperature-from-example-com
  (with-fake-http
    (into
      example-com-response-ok
      openweathermap-org-response-ok)
    (let [responses (provider/request-temperatures providers)
          {:keys [temperature]} (first responses)]
      (is (= example-com-temperature temperature)))))

(deftest test-returns-temperature-from-openweathermap-org
  (with-fake-http
    (into
      example-com-response-ok
      openweathermap-org-response-ok)
    (let [responses (provider/request-temperatures providers)
          {:keys [temperature]} (second responses)]
      (is (= openweathermap-org-temperature temperature)))))
