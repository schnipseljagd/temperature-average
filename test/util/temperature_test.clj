(ns util.temperature-test
  (:require [clojure.test :refer :all]
            [util.temperature :refer :all]))

(deftest test-absolute-zero?
  (is (true? (lesser-absolute-zero? -273.16)))
  (is (false? (lesser-absolute-zero? -273.15)))
  (is (false? (lesser-absolute-zero? 0))))
